/*
 * fetch json and show on embed on index.html
 * see NOTES.txt for more.
 */
const userAction = async () => {
  const response = await fetch('https://multiexch.com:3448/api/d_rate/teen9');
  const myJson = await response.json(); //extract JSON from the http response
  // do something with myJson
  // 
  //document.getElementById("par1").innerHTML = JSON.parse(myJson);
  console.log(myJson);

  str = JSON.stringify(myJson);
  str = JSON.stringify(myJson,null,4);
  document.getElementById("output").innerHTML = str;


  //myJson stores the json value

  //extracting values of gtype
  var gametype = myJson["gtype"];
  //printing gtype on html
  document.getElementById("gametype").innerHTML = gametype;

  //extracting values of c1
  //for ease of understanding, i'm extracting c1 in multiple steps
  //Reminder: do these in one line
  var j_data = myJson["data"];//value of "data"
  var t_1 = j_data["t1"];//value of t1, which is an array of a dictionary
  var t_1_dictionary = t_1[0];//extracting the dictionary
  var c_1 = t_1_dictionary["C1"];
  //print the value of c1 on html 
  document.getElementById("C1").innerHTML = c_1;

}

//refresh data every 2 seconds by calling userAction() every 2 seconds
setInterval(userAction, 2000);

userAction();

 /* Note: 
  * extracting data from json
  * variable "myJson" contains the fetched api object data
  * let say 
  * myJson = { "key1": 1, "key2": 2, "key3": [{"k1": 11, "k2": 12}]
  }
  * then
    value_key1 = myJson["key1"];// 1
    value_key3 = myJson["key3"];//[{"k1": 11, "k2": 12}]
    value_k1 = ((myJson["key3"])[0])["k1"];// 11
  */